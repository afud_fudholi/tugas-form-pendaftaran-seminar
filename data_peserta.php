<?php  
require 'functions.php';
$no=1;
$peserta = query("SELECT * FROM peserta");


// tombol cari ditekan
if( isset($_POST["cari"]) ) {
	$peserta = cari($_POST["keyword"]);
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Peserta</title>
	<style>
		body {
			background-color: #f6f6f6;
		}
	</style>
</head>
<body>
<h2 align="center">Data Peserta</h2>
<form action="" method="post">
	<input type="text" name="keyword" size="40" autofocus placeholder="masukkan keyword pencarian..." autocomplete="off">
	<button type="submit" name="cari">Cari!</button>
</form>
<table border="1" width="100%">
	<tr>
			<td>No</td>
			<td>Nama</td>
			<td>Email</td>
			<td>No Telepon</td>
			<td>Tempat Lahir</td>
			<td>Tanggal Lahir</td>
			<td>Jenis Kelamin</td>
			<td>Alamat</td>
			<td>Jenis Instansi</td>
			<td>Nama Instansi</td>
			<td>Keterangan</td>
			<td>Aksi</td>
	</tr>

<?php foreach ($peserta as $row) : ?>
		<tr>
			<td><?php echo $no++;?></td>
			<td><?= $row["nama"]; ?></td>
			<td><?= $row["email"]; ?></td>
			<td><?= $row["no_tlp"]; ?></td>
			<td><?= $row["tem_lahir"]; ?></td>
			<td><?= $row["tgl_lahir"]; ?></td>
			<td><?= $row["jk"]; ?></td>
			<td><?= $row["alamat"]; ?></td>
			<td><?= $row["jns_instansi"]; ?></td>
			<td><?= $row["nm_instansi"]; ?></td>
			<td><?= $row["ket"]; ?></td>

			<td><!-- <a href="details_peserta.php?id=<?= $row['id'];?>">Details</a> -->
				<a href="edit_peserta.php?id=<?= $row["id"]; ?>">Edit</a>
				<a href="hapus_peserta.php?id=<?= $row['id']; ?>">Hapus</a>
			</td>
		</tr>
<?php endforeach; ?>
</table>
</body>
</html>