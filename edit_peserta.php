<?php 
require 'functions.php';

//ambil data di URL
$id = $_GET["id"];

// query data seminar berdasarkan id
$smr = query("SELECT * FROM peserta WHERE id = $id")[0];

// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {

	// cek apakah data berhasil diedit atau tidak
	if( ubah($_POST) > 0 ) {
		echo "
			<script>
				alert('Data berhasil diedit');
				document.location.href = 'data_peserta.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('Data gagal diedit');
				document.location.href = 'data_peserta.php';
			</script>
		";
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Peserta</title>
	<style>
		body {
			background-color: #f6f6f6;
		}
	</style>
</head>
<body>
<h2>Edit Data Peserta</h2>
<form action="" method="POST">
	<input type="hidden" name="id" value="<?= $smr["id"] ?>">
		<table align="center">

			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama" id="nama" required value="<?= $smr["nama"] ?>"></td>
			</tr>	
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="email" name="email" id="email" required value="<?= $smr["email"] ?>"></td>
			</tr>
			<tr>
				<td>No Telepon</td>
				<td>:</td>
				<td><input type="text" name="no_tlp" id="no_tlp" value="<?= $smr["no_tlp"] ?>"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" name="tem_lahir" id="tem_lahir" value="<?= $smr["tem_lahir"] ?>"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><input type="date" name="tgl_lahir" id="tgl_lahir" value="<?= $smr["tgl_lahir"] ?>"></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td><input type="radio" name="jk" value="Laki_laki" value="Laki-Laki">Laki-Laki
				<input type="radio" name="jk" value="Perempuan" value="Perempuan">Perempuan
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><textarea name="alamat" value="<?= $smr["alamat"] ?>"></textarea></td>
			</tr>
			<tr>
				<td>Jenis Instansi</td>
				<td>:</td>
				<td><select name="jns_instansi" id="jns_instansi">
    			<option name="jns_instansi" value="Badan khusus">Badan Khusus</option>
    			<option name="jns_instansi" value="Negara">Negara</option>
    			<option name="jns_instansi" value="Swasta">Swasta</option>
			    <option name="jns_instansi" value="perorangan">Perorangan</option>
 				</select></td>
 			</tr>
 			<tr>
				<td>Nama Instansi</td>
				<td>:</td>
				<td><input type="text" name="nm_instansi" id="nm_instansi" required value="<?= $smr["nm_instansi"] ?>"></td><br><br>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td>:</td>
				<td><textarea name="ket" required value="<?= $smr["ket"] ?>"></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><button type="submit" name="submit">Edit</button>
			</tr>
		</table>	
	</form>
</body>
</html>