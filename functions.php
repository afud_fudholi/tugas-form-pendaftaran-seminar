<!-- database tugas = seminar
tabel  seminar = peserta
tampilan    =   data_peserta
 -->
<?php 

$conn = mysqli_connect("localhost", "root", "", "seminar");


function query($query){
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while ( $row = mysqli_fetch_assoc($result) ) {
		$rows[] = $row;
	}
	return $rows;
}




function tambah($data) {
	global $conn;

	//ambil data dari tiap elemen dalam form
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$no_tlp = htmlspecialchars($data["no_tlp"]);
	$tem_lahir = htmlspecialchars($data["tem_lahir"]);
	$tgl_lahir = htmlspecialchars($data["tgl_lahir"]);
	$jk = htmlspecialchars($data["jk"]);
	$alamat = htmlspecialchars($data["alamat"]);
	$jns_instansi = $data["jns_instansi"];
	$nm_instansi = $data["nm_instansi"];
	$ket = htmlspecialchars($data["ket"]);


	// query insert data
	$query = "INSERT INTO peserta
				VALUES
				('', '$nama', '$email', '$no_tlp', '$tem_lahir', '$tgl_lahir', $jk, '$alamat', '$jns_instansi', '$nm_instansi', '$ket')
				";
	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}



function hapus($id) {
	global $conn;

	mysqli_query($conn, "DELETE FROM peserta WHERE id = $id");

	return mysqli_affected_rows($conn);
}



function ubah($data) {
	global $conn;

	//ambil data dari tiap elemen dalam form
	$id = $data["id"];
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$no_tlp = htmlspecialchars($data["no_tlp"]);
	$tem_lahir = htmlspecialchars($data["tem_lahir"]);
	$tgl_lahir = htmlspecialchars($data["tgl_lahir"]);
	$jk = htmlspecialchars($data["jk"]);
	$alamat = htmlspecialchars($data["alamat"]);
	$jns_instansi = htmlspecialchars($data["jns_instansi"]);
	$nm_instansi = htmlspecialchars($data["nm_instansi"]);
	$ket = htmlspecialchars($data["ket"]);


	// query insert data
	$query = "UPDATE peserta SET
				nama = '$nama',
				email = '$email',
				no_tlp = '$no_tlp',
				tem_lahir = '$tem_lahir',
				tgl_lahir = '$tgl_lahir',
				jk = '$jk',
				alamat = '$alamat',
				jns_instansi = '$jns_instansi',
				nm_instansi = '$nm_instansi',
				ket = '$ket'
			  WHERE id = $id
			";
	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}


function cari($keyword) {
	$query = "SELECT * FROM peserta
				WHERE
			  nama LIKE '%$keyword%' OR 
			  email LIKE '%$keyword%' OR
			  no_tlp LIKE '%$keyword%' OR 
			  tem_lahir LIKE '%$keyword%' OR
			  alamat LIKE '%$keyword%' OR 
			  jns_instansi LIKE '%$keyword%' OR
			  nm_instansi LIKE '%$keyword%'
			  
			";

	return query($query);
}
