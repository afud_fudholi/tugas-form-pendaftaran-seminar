<!-- <?php 
require 'functions.php';

// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {

	// cek apakah data berhasil di tambahkan atau tidak
	if( tambah($_POST) > 0 ) {
		echo "
			<script>
				alert('data berhasil ditambahkan!');
				document.location.href = 'data_peserta.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('data gagal ditambahkan!');
				document.location.href = 'data_peserta.php';
			</script>
		";
		echo mysqli_error($conn);
	}
}

?> -->


<!DOCTYPE html>
<html>
<head>
	<title>Form Pendaftaran Seminar </title>
	<style>
		body {
			background-color: #f6f6f6;
		}
	</style>
</head>
<body>
	<h2 align="center">Form Pendaftaran Seminar Digital Forensic</h2>
	<h3 align="center">"Uncovering Crime Using Digital Forensic"</h3>
	<form action="simpan_peserta.php" method="post">
		<table align="center">

			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama" id="nama" required></td>
			</tr>	
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="email" name="email" id="email" required></td>
			</tr>
			<tr>
				<td>No Telepon</td>
				<td>:</td>
				<td><input type="text" name="no_tlp" id="no_tlp"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" name="tem_lahir" id="tem_lahir"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><input type="date" name="tgl_lahir" id="tgl_lahir"></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td><input type="radio" name="jk" value="Laki_laki">Laki-Laki
				<input type="radio" name="jk" value="Perempuan">Perempuan
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><textarea name="alamat"></textarea></td>
			</tr>
			<tr>
				<td>Jenis Instansi</td>
				<td>:</td>
				<td><select name="jns_instansi" id="jns_instansi">
    			<option name="jns_instansi" value="Badan khusus">Badan Khusus</option>
    			<option name="jns_instansi" value="Negara">Negara</option>
    			<option name="jns_instansi" value="Swasta">Swasta</option>
			    <option name="jns_instansi" value="perorangan">Perorangan</option>
 				</select></td>
 			</tr>
 			<tr>
				<td>Nama Instansi</td>
				<td>:</td>
				<td><input type="text" name="nm_instansi" id="nm_instansi" required></td><br><br>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td>:</td>
				<td><textarea name="ket"></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><button type="submit" name="submit">Daftar</button></td>
			</tr>
		</table>	
	</form>
</body>
</html>